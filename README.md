# Weather Station

Weather Station is a (yet another) weather data gathering project.
This weather station project aims to incorporate Raspberry Pi's, Arduino's and a wide variety of sensors into an easy-configurable network.

## Getting Started

For ease of use, using an Anaconda distribution is highly suggested.
Nevertheless, one can build an environment by installing the individual packages and their prerequisites.

The software was tested against an Ubuntu 18.04, Ubuntu 19.10 and Windows 10 systems and found to be working. 

### Prerequisites

* Python 3 - Python is used as the main driving language for the GUI and the logic.
* PyQt5 - The GUI engine.
* Matplotlib - Used for 2D visualizations.
* pandas - Used for parsing layers from *.csv files.
* Spyder - Not required but a very helpful and powerful IDE. Any other IDE/editor of choice can be used.

* Arduino IDE - (or any other) a software to communicate and upload sketches to the Arduino microcontroller (MC).

### Installing

To set up the software, install Anaconda on your machine from [the Anaconda website](https://www.anaconda.com/distribution/).
Follow the installation instructions.

When Anaconda is installed, open the Anaconda Prompt and prepend "conda-forge" to the channels list:
```
conda config --prepend channels conda-forge
```

Then update and rebase the packages to the top channel by running:
```
conda update --all
```

To install and update the required packages run the following command:
```
conda install python pyqt5 matplotlib numpy pandas spyder
```

When the installation is over, all the required packages should be installed and the environment is ready.

Do note, that these instructions suggest installation of packages in the main (base) environment.
One is free and encouraged to create specific environments for each project.

Arduino IDE is available from the [Arduino official website](https://www.arduino.cc/en/Main/Software)

Additional Arduino libraries are included in the _microcontroller/Arduino folder.


### Running an example.
TBD.


## Authors
** Dima Silbermann ** as [Dimonade](https://gitlab.com/Dimonade).




