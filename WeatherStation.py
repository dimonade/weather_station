import sys
rec_lim = sys.getrecursionlimit()
print("Recursion limit: {}.".format(sys.getrecursionlimit()))
if rec_lim > 50000:
    sys.setrecursionlimit(50000)
else:
    sys.setrecursionlimit(rec_lim*5)
print("Updated recursion limit: {}.".format(sys.getrecursionlimit()))

import platform
platform_type = platform.platform()
print("Platform: {}.".format(platform_type))

from PyQt5.QtWidgets import QApplication, QMainWindow

import WeatherStationModel
import WeatherStationUIView

class WeatherStation(QMainWindow):
    def __init__(self, parent=None):
        super(WeatherStation, self).__init__(parent)
        self.view = WeatherStationUIView.WeatherStationUIView()
        self.view.setupUi(self)
        
    def showMaximized(self):
        self.view.showMaximized()


if __name__ == "__main__":
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance()
    
    modelForm = WeatherStationModel.WeatherStationModel()
    form = WeatherStationUIView.WeatherStationUIView(model=modelForm)
    modelForm.setView(ui=form)
    
    form.show()
    # form.showMaximized()
    app.exec_()